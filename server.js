const mqtt = require('mqtt');
const MapboxClient = require('mapbox');
const express = require('express');
const path = require('path');
const pug = require('pug');

var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var vehicle_emit = [];
var vehicles = new Array();

server.listen(8001);


app.set('view engine', pug);

app.use(express.static(path.join(__dirname, 'views')));

app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname + '/index.html'));
});

app.listen(8000, function() {
	console.log("\x1b[36m","[HTTP]", "\x1b[0m", "Server is listening on port 8000");
});

app.use(express.static(path.join(__dirname, 'views')));

app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname + '/index.html'));
});

io.on('connection', function(socket) {
  console.log("\x1b[35m", "[Socket.io]", "\x1b[0m", "Client connected");
  setInterval(function(){
    io.emit('newStatus', vehicles)
    //io.emit('status', vehicle_emit);
  }, 1000);
  //io.emit('status', vehicle_emit);
});

function Epoch(date) {
  return Math.round(new Date(date).getTime() / 1000.0);
}


function garbageCollector() {
  console.log('\x1b[33m', "[DEBUG]", "\x1b[0m", "Garbagecollector()");
  var epoch = Epoch(new Date());
  vehicles.forEach(function(value, key) {
    if(value.next_stop == "EOL") {
      vehicles.splice(key, 1);
      console.log('\x1b[33m', "[DEBUG]", "\x1b[0m", "Removed vehicle from key " + key + " (E0L)");
    } else if(value.tsi  < (epoch - 60)) {
      vehicles.splice(key, 1);
      console.log('\x1b[33m', "[DEBUG]", "\x1b[0m", "Removed vehicle from key " + key + " (Vehicle too old)");
    }
  });
}


// Debug text: console.log('\x1b[33m', "[DEBUG]", "\x1b[0m", "Connection established");
// v1
//<prefix><version>/journey/<temporal_type>/<transport_mode>/<operator_id>/<vehicle_number>/<route_id>/<direction_id>/<headsign>/<start_time>/<next_stop>/<geohash_level>/<geohash>/
// v2
///<prefix>/<version>/<journey_type>/<temporal_type>/<event_type>/<transport_mode>/<operator_id>/<vehicle_number>/<route_id>/<direction_id>/<headsign>/<start_time>/<next_stop>/<geohash_level>/<geohash>/#
//<  0   ><   1   >/   2           /      3        /        4   /       5        /      6      /     7          /      8   /     9       /    10       /     11    /       12      /   13    /  
// v1: /hfp/v1/journey/ongoing/tram/0040/+/+/+/+/+/+/+/#
// v2: /hfp/v2/journey/ongoing/vp/+/+/+/+/+/+/+/+/0/#
const topic = '/hfp/v2/journey/ongoing/vp/tram/0040/+/+/+/+/+/+/+/#';


const client  = mqtt.connect('mqtts://mqtt.hsl.fi:8883');

client.on('connect', function () {
  client.subscribe(topic);
  console.log('\x1b[32m', "[MQTT]", "\x1b[0m", "Connection established");
  setInterval(garbageCollector, 15000);
});
 
var count = 0;

client.on('error', function() {
  console.log('\x1b[32m', "[MQTT]", "\x1b[0m", "Error encountered");
});

client.on('message', function (topic, message) {
  var split = topic.split("/");
  if(count < 5) {
    console.log(topic);
    count++;
  }
  const vehicle_position = JSON.parse(message).VP;

  



    //Skip vehicles with invalid location
    if (!vehicle_position.lat || !vehicle_position.long) {
      return;
    } else {
      vehicle_emit = vehicle_position;
      processData(vehicle_emit, split);
    }
    

});


/*
    vehicle object construction:
    https://digitransit.fi/en/developers/apis/4-realtime-api/vehicle-positions/#the-payload

    most important:
    @desi     - destination that can be seen by passengers (e.g. 6T)
    @dir      - direction the tram is traveling (1 - out of city, 2 - into city)
    @veh      - vehicle number painted on vehicle
    @lat/long - positional coordinates
    @tsi - epoch timestamp
*/

function findPopulatedIndex(key) {
  return vehicles.map(function(o) { return o.veh }).indexOf(key);
}


function processData(data, topic) {
    var index = findPopulatedIndex(data.veh);
    if(index != -1) {
      vehicles.splice(index, 1);
    }
      vehicle = {
        veh: data.veh,
        dir: data.dir,
        desi: data.desi,
        lat: data.lat,
        long: data.long,
        start: data.start,
        tsi: data.tsi,
        dest: topic[11],
        next_stop: topic[13],
      };
    vehicles.push(vehicle);
    return vehicles;
};

/*
  Array on server side => Constantly updates
  Push array to client every 1-5 seconds (depending on load)
  Proceed to update client based on array

  Cleanup of unused data? 
  // Fixed as of 1.8.2019
    - Switched to using standard array keys instead of vehicle-ID based keys. (Reduced a significant amount of lag due to having to process ~400 empty arrays a query. Switching load more towards the server instead of the client.)
*/

