
/*
    vehicle object construction:
    https://digitransit.fi/en/developers/apis/4-realtime-api/vehicle-positions/#the-payload

    most important:
    @desi     - destination that can be seen by passengers (e.g. 6T)
    @dir      - direction the tram is traveling (1 - out of city, 2 - into city)
    @veh      - vehicle number painted on vehicle
    @lat/long - positional coordinates
    @
*/

var objects = [];
var lineObjects = [];
var n_objects = [];
var map = null;

function simplifyDirection(direction) {
    if(direction == "1") {
        return "maalle";
    } else if(direction === "2") {
        return "merelle";
    } else {
        return "ei tiedossa";
    }
}

/*

    STOP: DID YOU REMEMBER TO CHANGE THE EMIT IP TO DEV MACHINE!?!?!?

*/

function generatePopup(data) {
    var popup = new mapboxgl.Popup({offset: 25}).setHTML('\
    <b>' + data.dest  + ' (' + data.desi + ')</b><br>\
    Runko ' + data.veh + '<br>\
    Suunta <u>' + simplifyDirection(data.dir) + '</u><br>\
    Lähtöaika ' + data.start + '<br>\
    ');
    var el = document.createElement('div');  
    el.id = 'popup-' + data.veh;
    return popup;
}

function Epoch(date) {
   return Math.round(new Date(date).getTime() / 1000.0);
}

function n_garbageCollector() {
    var epoch = Epoch(new(Date));
    $.each(n_objects, function(key, value) {    
        if((value.tsi - epoch)  > 60) { // 1 > 2
            var marker = value.marker;
            marker.remove();
            n_objects.splice(key, 1);
            console.log("Time difference: " + (value.ts - epoch) + " seconds");
            console.log("Destroyed vehicle at index " + key);
        } else if(value.next_stop == "EOL") {
            var marker = value.marker;
            marker.remove();
            n_objects.splice(key, 1);
            console.log("Destroyed vehicle at index " + key + " (EOL)");
        }
    });
}

function findPopulatedIndex(key) {
    return n_objects.map(function(o) { return o.id }).indexOf(key);
}

/*
    Issue: When storing the current marker and object in array, it (obviously) gets deleted as soon as the new pos-data comes into view.
    Solution: Generate separate array, where to store these objects and data, then fetch that object based on key and change the position based on that.

*/
function n_loadObjects(data) {
    $("#vehicleCount").text(n_objects.length);
    $.each(data, function(key, value) {
        if(value != null) {
            index = findPopulatedIndex(value.veh);
            if(index == -1) {

                // object does not exist in array, generate new object
                var veh = {
                    id: value.veh,
                    veh: value.veh,
                    dir: value.dir,
                    lat: value.lat,
                    long: value.long,
                    start: value.start,
                    desi: value.desi,
                    dest: value.dest,
                    next_stop: value.next_stop,
                    tsi: value.tsi,
                }
                var n_array = n_objects.push(veh) - 1;
                n_generateVehicle(veh, n_array);
            } else {
                
                //console.log(n_objects[index]);
                n_objects[index]['lat'] = value.lat;
                n_objects[index]['long'] = value.long;
                n_updateVehicle(n_objects[index], index);
            }
        }
    });
}


function n_updateVehicle(vehicle, index) {
    var element = document.querySelector('.vehicle-icon-' + vehicle.veh);
    if(element) {
        var location = vehicle.marker;
        var popup = vehicle.popup;
        if(popup) {
            popup.setHTML('\
            <b>' + vehicle.dest  + ' (' + vehicle.desi + ')</b><br>\
            Runko ' + vehicle.veh + '<br>\
            Suunta <u>' + simplifyDirection(vehicle.dir) + '</u><br>\
            Lähtöaika ' + vehicle.start + '<br>\
            ');
        }
        location.setLngLat([vehicle.long, vehicle.lat]);
        vehicle.ts = Epoch(new Date());
    }
}


    
function n_generateVehicle(vehicle, key) {
    function createDatalabel() {
        var lineLabel = document.createElement('div');
        lineLabel.className = 'line-number';
        lineLabel.innerHTML = vehicle.desi + "<br><span class='vehicle_id'>" + vehicle.veh + '</span>';
        return [lineLabel.outerHTML];
    }
    // Icon function
    var labelContent = document.createElement('div');
    labelContent.className = 'vehicle-icon-' + vehicle.veh;
    labelContent.style.backgroundColor = colorMapLines(vehicle.desi);
    labelContent.innerHTML = createDatalabel().join('');
    // Popup generation
    popupid = generatePopup(vehicle);
    // Mabox marker
    var location = new mapboxgl.Marker({
        type: 'vehicle',
        element: labelContent,
        anchor: 'center',
        color: "#3FB1CE"
    }).setLngLat([vehicle.long, vehicle.lat]).setPopup(popupid).addTo(map); 
    // Store mapbox data in objects
    n_objects[key]['marker'] = location;
    n_objects[key]['popup'] = popupid;

} 


// Generate colors for each tram line that is currently in use
// TODO: Generate the same for trams going into the garage

function colorMapLines(line) {
    switch(line) {
        case '1':
            return "#00BBE8";
        case '2':
            return "#60BB46";
        case '3':
            return "#0087CD";
        case '4':
            return "#ED1E60";
        case '5':
            return "#6C6D70";
        case '6':
            return "#00A665";
        case '6T':
            return "#00A665";
        case '7':
            return "#D50D8C";
        case '8':
            return "#7960AA";
        case '9':
            return "#F198C1";
        case '10':
            return "#FCB615";
        default:
            return "#6C6D70";
    }
}

function loadMap() {
    mapboxgl.accessToken = 'pk.eyJ1IjoiYWxleGhhYWphIiwiYSI6ImNqd3ppaGZtMTExNGg0YXBtZ3F4ZnE5OTkifQ.mxv5PsuCIvZLwfavBZ5RqA';

    // Limit the map's scrolling boundaries to innder helsinki, inside ring 1
    var bounds = [
        [24.837167, 60.143938], // Southwest coordinates
        [25.001957, 60.222033]  // Northeast coordinates
        ];

    map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/streets-v9',
        center: [24.945831, 60.172059],
        zoom: 12,
        maxBounds: bounds,
        AttributionControl: false
        });

    map.on('load', function() {
        viewMap();
    });

    map.addControl(new mapboxgl.GeolocateControl({
        positionOptions: {
            enableHighAccuracy: true
        },
        trackUserLocation: true,
    }));
}

function viewMap() {
    $("#loading").fadeOut();
    $("#map").css('visibility', 'visible').hide().fadeIn(500);
}

function loadSystem() {
    socket = io('http://192.168.5.61:8001');
    socket.on('newStatus', n_loadObjects);
    loadMap();
}

$(function() {
    loadSystem();
    setInterval(n_garbageCollector, 30000);
});